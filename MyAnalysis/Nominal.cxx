#define Nominal_cxx
// The class definition in Nominal.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.


// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("Nominal.C")
// root> T->Process("Nominal.C","some options")
// root> T->Process("Nominal.C+")
//

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "Nominal.h"
#include <TH2.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <math.h>

#include <TSystemDirectory.h>
#include <TSystem.h>

void Nominal::Begin(TTree * /*tree*/)
{
  // The Begin() function is called at the start of the query.
  // When running with PROOF Begin() is only called on the client.
  // The tree argument is deprecated (on PROOF 0 is passed).

  
  // Create all the histograms and set up features
  TString option = GetOption();
  cutflow_Vqq = makeHist("cutflow_Vqq",12,-2,10);
  cutflow_Vbb = makeHist("cutflow_Vbb",12,-2,10);
  jet_pt = makeHist("jet_pt",100,0.,3000.);
  jet_mass = makeHist("jet_mass",100,0.,500.);
  largeRjet_pt = makeHist("largeRjet_pt",100,200.,3000.);
  largeRjet_mass = makeHist("largeRjet_mass",250,0.,500.);
  
  trackjet_pt = makeHist("trackjet_pt",100,0.,3000.);
  trackjet_mass = makeHist("trackjet_mass",500,0.,500.);
  muon_pt = makeHist("muon_pt",100,0.,3000.);
  ntrk_hist = makeHist("ntrk",20,0.,20.);
  
  Selected_leading_largeRjet_mass = makeHist("Selected_leading_largeRjet_mass",100,0.,500.);
  Selected_leading_largeRjet_pt = makeHist("Selected_leading_largeRjet_pt",100,200.,3000.);
  Selected_subleading_largeRjet_mass = makeHist("Selected_subleading_largeRjet_mass",100,0.,500.);
  Selected_subleading_largeRjet_pt = makeHist("Selected_subleading_largeRjet_pt",100,200.,3000.);
  Selected_largeRjet_deltaR = makeHist("Selected_largeRjet_deltaR",50,1.,6.);

  Selected_pTsymmetry_leading_largeRjet_mass = makeHist("Selected_pTsymmetry_leading_largeRjet_mass",100,0.,500.);
  Selected_pTsymmetry_leading_largeRjet_pt = makeHist("Selected_pTsymmetry_leading_largeRjet_pt",100,200.,3000.);
  Selected_pTsymmetry_subleading_largeRjet_mass = makeHist("Selected_pTsymmetry_subleading_largeRjet_mass",100,0.,500.);
  Selected_pTsymmetry_subleading_largeRjet_pt = makeHist("Selected_pTsymmetry_subleading_largeRjet_pt",100,200.,3000.);
  Selected_pTsymmetry_largeRjet_deltaR = makeHist("Selected_pTsymmetry_largeRjet_deltaR",50,1.,6.);

  Selected_pTsymmetry_etaCut_leading_largeRjet_mass = makeHist("Selected_pTsymmetry_etaCut_leading_largeRjet_mass",100,0.,500.);
  Selected_pTsymmetry_etaCut_leading_largeRjet_pt = makeHist("Selected_pTsymmetry_etaCut_leading_largeRjet_pt",100,200.,3000.);
  Selected_pTsymmetry_etaCut_subleading_largeRjet_mass = makeHist("Selected_pTsymmetry_etaCut_subleading_largeRjet_mass",100,0.,500.);
  Selected_pTsymmetry_etaCut_subleading_largeRjet_pt = makeHist("Selected_pTsymmetry_etaCut_subleading_largeRjet_pt",100,200.,3000.);
  Selected_pTsymmetry_etaCut_largeRjet_deltaR = makeHist("Selected_pTsymmetry_etaCut_largeRjet_deltaR",500,1.,6.);

  //Wtagged_smooth50_mass = makeHist("Wtagged_smooth50_mass",100,20.,500.);
  //Wtagged_smooth50_pt = makeHist("Wtagged_smooth50_pt",100,200.,3000.);

  Wtagged_smooth80_mass = makeHist("Wtagged_smooth80_mass",24,30.,150.);
  Wtagged_smooth80_pt = makeHist("Wtagged_smooth80_pt",100,200.,3000.);

  //Ztagged_smooth50_mass = makeHist("Ztagged_smooth50_mass",100,20.,500.);
  //Ztagged_smooth50_pt = makeHist("Ztagged_smooth50_pt",100,200.,3000.);

  Ztagged_smooth80_mass = makeHist("Ztagged_smooth80_mass",24,30.,150.);
  Ztagged_smooth80_pt = makeHist("Ztagged_smooth80_pt",100,200.,3000.);

  Tagged_mass = makeHist("Tagged_mass",24,30.,150.);
  Tagged_pt = makeHist("Tagged_pt",100,200.,3000.);
  Tagged_ntrk = makeHist("Tagged_ntrk",20,0.,20.);
  Tagged_leadtrk_pt = makeHist("Tagged_leadtrk_pt",100,0.,500.);
  Tagged_subtrk_pt = makeHist("Tagged_subtrk_pt",100,0.,300.);
  Tagged_deltaR = makeHist("Tagged_deltaR",10,0.,1.);

  Zcand_mass = makeHist("Zcand_mass",20,50.,150.);
  Zcand_pt = makeHist("Zcand_pt",100,200.,3000.);
  Zcand_ntrk = makeHist("Zcand_ntrk",20,0.,20.);
  Zcand_leadtrk_pt = makeHist("Zcand_leadtrk_pt",100,0.,500.);
  Zcand_subtrk_pt = makeHist("Zcand_subtrk_pt",100,0.,300.);
  Zcand_deltaR = makeHist("Zcand_deltaR",10,0.,1.);

  Zcand_0b_MV2_mass = makeHist("Zcand_0b_MV2_mass",20,50.,150.);
  Zcand_0b_MV2_pt = makeHist("Zcand_0b_MV2_pt",100,200.,3000.);
  Zcand_0b_MV2_leadtrk_pt = makeHist("Zcand_0b_MV2_leadtrk_pt",100,0.,500.);
  Zcand_0b_MV2_subtrk_pt = makeHist("Zcand_0b_MV2_subtrk_pt",100,0.,300.);
  Zcand_0b_MV2_deltaR = makeHist("Zcand_0b_MV2_deltaR",10,0.,1.);
  
  /*// Histograms for 60%  
  Tagged_1b_MV2_60_mass = makeHist("Tagged_1b_MV2_60_mass",100,20.,500.);
  Tagged_1b_MV2_60_pt = makeHist("Tagged_1b_MV2_60_pt",100,200.,3000.);
  Tagged_1b_MV2_60_leadtrk_pt = makeHist("Tagged_1b_MV2_60_leadtrk_pt",100,0.,500.);
  Tagged_1b_MV2_60_subtrk_pt = makeHist("Tagged_1b_MV2_60_subtrk_pt",100,0.,300.);
  Tagged_1b_MV2_60_deltaR = makeHist("Tagged_1b_MV2_60_deltaR",100,0.,1.);
  
  Tagged_2b_MV2_60_mass = makeHist("Tagged_2b_MV2_60_mass",100,20.,500.);
  Tagged_2b_MV2_60_pt = makeHist("Tagged_2b_MV2_60_pt",100,200.,3000.);
  Tagged_2b_MV2_60_leadtrk_pt = makeHist("Tagged_2b_MV2_60_leadtrk_pt",100,0.,500.);
  Tagged_2b_MV2_60_subtrk_pt = makeHist("Tagged_2b_MV2_60_subtrk_pt",100,0.,300.);
  Tagged_2b_MV2_60_deltaR = makeHist("Tagged_2b_MV2_60_deltaR",100,0.,1.);*/

  // Histograms for 70%
  Zcand_1b_MV2_70_mass = makeHist("Zcand_1b_MV2_70_mass",20,50.,150.);
  Zcand_1b_MV2_70_pt = makeHist("Zcand_1b_MV2_70_pt",100,200.,3000.);
  Zcand_1b_MV2_70_leadtrk_pt = makeHist("Zcand_1b_MV2_70_leadtrk_pt",100,0.,500.);
  Zcand_1b_MV2_70_subtrk_pt = makeHist("Zcand_1b_MV2_70_subtrk_pt",100,0.,300.);
  Zcand_1b_MV2_70_deltaR = makeHist("Zcand_1b_MV2_70_deltaR",10,0.,1.);

  Zcand_2b_MV2_70_mass = makeHist("Zcand_2b_MV2_70_mass",20,50.,150.);
  Zcand_2b_MV2_70_pt = makeHist("Zcand_2b_MV2_70_pt",100,200.,3000.);
  Zcand_2b_MV2_70_leadtrk_pt = makeHist("Zcand_2b_MV2_70_leadtrk_pt",100,0.,500.);
  Zcand_2b_MV2_70_subtrk_pt = makeHist("Zcand_2b_MV2_70_subtrk_pt",100,0.,300.);
  Zcand_2b_MV2_70_deltaR = makeHist("Zcand_2b_MV2_70_deltaR",10,0.,1.);

  // Histograms for 77%
  /*Selected_1b_MV2_77_mass = makeHist("Selected_1b_MV2_77_mass",100,20.,500.);
  Selected_1b_MV2_77_pt = makeHist("Selected_1b_MV2_77_pt",100,200.,3000.);
  Selected_1b_MV2_77_leadtrk_pt = makeHist("Selected_1b_MV2_77_leadtrk_pt",100,0.,500.);
  Selected_1b_MV2_77_subtrk_pt = makeHist("Selected_1b_MV2_77_subtrk_pt",100,0.,300.);
  Selected_1b_MV2_77_deltaR = makeHist("Selected_1b_MV2_77_deltaR",100,0.,1.);

  Selected_2b_MV2_77_mass = makeHist("Selected_2b_MV2_77_mass",100,20.,500.);
  Selected_2b_MV2_77_pt = makeHist("Selected_2b_MV2_77_pt",100,200.,3000.);
  Selected_2b_MV2_77_leadtrk_pt = makeHist("Selected_2b_MV2_77_leadtrk_pt",100,0.,500.);
  Selected_2b_MV2_77_subtrk_pt = makeHist("Selected_2b_MV2_77_subtrk_pt",100,0.,300.);
  Selected_2b_MV2_77_deltaR = makeHist("Selected_2b_MV2_77_deltaR",100,0.,1.);
  */
  // Histograms for 85%
  Zcand_1b_MV2_85_mass = makeHist("Zcand_1b_MV2_85_mass",20,50.,150.);
  Zcand_1b_MV2_85_pt = makeHist("Zcand_1b_MV2_85_pt",100,200.,3000.);
  Zcand_1b_MV2_85_leadtrk_pt = makeHist("Zcand_1b_MV2_85_leadtrk_pt",100,0.,500.);
  Zcand_1b_MV2_85_subtrk_pt = makeHist("Zcand_1b_MV2_85_subtrk_pt",100,0.,300.);
  Zcand_1b_MV2_85_deltaR = makeHist("Zcand_1b_MV2_85_deltaR",10,0.,1.);

  Zcand_2b_MV2_85_mass = makeHist("Zcand_2b_MV2_85_mass",20,50.,150.);
  Zcand_2b_MV2_85_pt = makeHist("Zcand_2b_MV2_85_pt",100,200.,3000.);
  Zcand_2b_MV2_85_leadtrk_pt = makeHist("Zcand_2b_MV2_85_leadtrk_pt",100,0.,500.);
  Zcand_2b_MV2_85_subtrk_pt = makeHist("Zcand_2b_MV2_85_subtrk_pt",100,0.,300.);
  Zcand_2b_MV2_85_deltaR = makeHist("Zcand_2b_MV2_85_deltaR",10,0.,1.);

  largeRjet_mass_vs_pt = makeHist2("largeRjet_mass_vs_pt",100,200.,3000.,150,0.,150.);
  Selected_leading_largeRjet_mass_vs_pt = makeHist2("Selected_leading_largeRjet_mass_vs_pt",100,200.,3000.,150,0.,150.);
}

void Nominal::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();
}

Bool_t Nominal::Process(Long64_t entry)
{
   // The Process() function is called for each entry in the tree (or possibly
   // keyed object in the case of PROOF) to be processed. The entry argument
   // specifies which entry in the currently loaded tree is to be processed.
   // When processing keyed objects with PROOF, the object is already loaded
   // and is available via the fObject pointer.
   //
   // This function should contain the \"body\" of the analysis. It can contain
   // simple or elaborate selection criteria, run algorithms on the data
   // of the event and typically fill histograms.
   //
   // The processing can be stopped by calling Abort().
   //
   // Use fStatus to set the return value of TTree::Process().
   //
   // The return value is currently not used.

   fReader.SetEntry(entry);
   
   float totalWeight = 1;
   float xs = 0;
   float filteff = 0;
   bool WqqSamples = 0;
   bool ZqqSamples = 0;
   bool ttbarSamples = 0;
   bool QCDSamples = 0;
   bool isZTag = 0;
   bool isWTag = 1;
   totalWeight = (*EventWeight)*mc_weighted/dsid_weights; // MC Weight
   //float totalWeight = 1; //Data Weight
   //std::cout<<mc_weighted<<std::endl;
   //std::cout<<dsid_weights<<std::endl;
   if((*DSID) >=364700 && (*DSID) < 370000) QCDSamples = 1;
   if((*DSID) >=410280 && (*DSID) < 410290) ttbarSamples = 1;
   if((*DSID) == 364375){
     xs = 0.016057;
     filteff = 1.0;
     ZqqSamples = 1;
   }
   if((*DSID) == 364376){
     xs = 0.0011984;
     filteff = 1.0;
     ZqqSamples = 1;
   }
   if((*DSID) == 364377){
     xs = 2.4103e-05;
     filteff = 1.0;
     ZqqSamples = 1;
   }
   if((*DSID) == 364378){
     xs = 0.037795;
     filteff = 1.0;
     WqqSamples = 1;
   }
   if((*DSID) == 364379){
     xs = 0.0028595;
     filteff = 1.0;
     WqqSamples = 1;
   }
   if((*DSID) == 364380){
     xs = 5.8377e-05;
     filteff = 1.0;
     WqqSamples = 1;
   }
   //totalWeight = (*EventWeight) *xs*filteff/(*EventNumber);
   //std::cout<<"totalWeight: " << totalWeight <<std::endl;

   std::vector<fatjet> largeRjets;
   std::vector<trackjet> trackjets;
   std::vector<muon> muons;
   int njets = JetIsSelected.GetSize();
   int nlargeRjets = LargeRJetIsSelected.GetSize();
   int ntrackjets = TrackJetIsSelected.GetSize();
   //int nphotons = PhotonIsSelected.GetSize();
   int nmuons = MuonIsSelected.GetSize();
   int nmuonsLoose = MuonIsSelectedLoose.GetSize();
   int nelectrons = ElectronIsSelected.GetSize();
   //std::cout<<"number of jets = "<<njets<<std::endl;
   //std::cout<<"number of largeRjets = "<<nlargeRjets<<std::endl;
   // std::cout<<"number of trackjets = "<<ntrackjets<<std::endl;
   std::cout<<"number of muons = "<<nmuons<<std::endl;
   std::cout<<"number of loose muons = "<<nmuonsLoose<<std::endl;
   std::cout<<"number of electrons = "<<nelectrons<<std::endl;

   cutflow_Vqq->Fill(-1.5,totalWeight);
   cutflow_Vbb->Fill(-1.5,totalWeight);
   for (int i=0;i<njets;++i) {
     if (JetIsSelectedLoose[i] == false) continue;
     jet_pt->Fill(JetPt[i],totalWeight);
     jet_mass->Fill(JetMass[i],totalWeight);
   } // Close for (int i=0;i<nJets;++i)
   // std::cout<<"vector of jets made"<<std::endl;
   if (WqqSamples || (QCDSamples && isWTag) || (ttbarSamples && isWTag)) {
     for (int i=0;i<nlargeRjets; ++i) { 
       fatjet temp;
       temp.SetPtEtaPhiM(LargeRJetPt[i],LargeRJetEta[i],LargeRJetPhi[i],LargeRJetMass[i]);
       temp.isPassNtrk = LargeRJetPassWNtrk[i];
       temp.isPassD2 = LargeRJetPassWD2[i];
       temp.isTagged = LargeRJetPassWD2[i] && LargeRJetPassWNtrk[i] && LargeRJetPassWMassHigh[i] && LargeRJetPassWMassLow[i]; 
       largeRjets.push_back(temp);
       largeRjet_pt->Fill(LargeRJetPt[i],totalWeight);
       largeRjet_mass->Fill(LargeRJetMass[i],totalWeight);
     } // Close for (int i=0;i<nlargeRjets;++i)
   }// Close if WqqSamples
   if (ZqqSamples || (QCDSamples && isZTag) || (ttbarSamples && isZTag)) {
     for (int i=0;i<nlargeRjets; ++i) {
       fatjet temp;
       temp.SetPtEtaPhiM(LargeRJetPt[i],LargeRJetEta[i],LargeRJetPhi[i],LargeRJetMass[i]);
       temp.isPassNtrk = LargeRJetPassZNtrk[i];
       temp.isPassD2 = LargeRJetPassZD2[i];
       temp.isTagged = LargeRJetPassZD2[i] && LargeRJetPassZNtrk[i] && LargeRJetPassZMassHigh[i] && LargeRJetPassZMassLow[i];
       largeRjets.push_back(temp);
       largeRjet_pt->Fill(LargeRJetPt[i],totalWeight);
       largeRjet_mass->Fill(LargeRJetMass[i],totalWeight);
       largeRjet_mass_vs_pt->Fill(LargeRJetPt[i],LargeRJetMass[i],totalWeight);
     } // Close for (int i=0;i<nlargeRjets;++i)
   }// Close if ZqqSamples
   //   std::cout<<"vector of largeRjets made"<<std::endl;

   for (int i=0;i<ntrackjets;++i) {  
     trackjet temp; 
     temp.SetPtEtaPhiM(TrackJetPt[i],TrackJetEta[i],TrackJetPhi[i],TrackJetMass[i]);
     temp.TrackJetIsBTagged_MV2c10_FixedCutBEff_85 = TrackJetIsBTagged_MV2c10_FixedCutBEff_85[i];
     temp.TrackJetIsBTagged_MV2c10_FixedCutBEff_77 = TrackJetIsBTagged_MV2c10_FixedCutBEff_77[i];
     temp.TrackJetIsBTagged_MV2c10_FixedCutBEff_70 = TrackJetIsBTagged_MV2c10_FixedCutBEff_70[i];
     temp.TrackJetIsBTagged_MV2c10_FixedCutBEff_60 = TrackJetIsBTagged_MV2c10_FixedCutBEff_60[i];
     trackjets.push_back(temp);
     trackjet_pt->Fill(TrackJetPt[i],totalWeight);
     trackjet_mass->Fill(TrackJetMass[i],totalWeight);
   } // Close for (int i=0;i<ntrackjets;++i)
   //std::cout<<"vector of trackjets made"<<std::endl;

   for (int i=0;i<nmuons;++i) {
     if (MuonIsSelectedLoose[i] == true) continue;
     muon temp;
     temp.SetPtEtaPhiM(MuonPt[i],MuonEta[i],MuonPhi[i],0.1056);
     muons.push_back(temp);
     muon_pt->Fill(MuonPt[i],totalWeight);
   } // Close for (int i=0;i<nmuons;++i)
   //   std::cout<<"vector of muons made"<<std::endl;

   FatToTrackLinks(largeRjets,trackjets,nlargeRjets);
   //   std::cout<<"FatToTrackLinks done"<<std::endl;
   EraseUnselectedFatjets(largeRjets);
   //   std::cout<<"Erased fatjets that aren't selected."<<std::endl;
   //muon-in-jet correction
   //fatjet *largeRjet = 0;
   //for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
   //  fatjet & largeR = (*lrj);
   //  largeRjet = &largeR;
   //  for (int i = 0; i < largeRjet->GetNumMuonVectors();++i) {
   //    muon * MuonInJet = largeRjet->GetMuonVetctor(i);

   for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
     fatjet & largeR = (*lrj);
     ntrk_hist->Fill(largeR.GetNumTrackVectors(),totalWeight);
   }
   // std::cout<<"ntrk filled"<<std::endl;

   fatjet * Tagged = 0;
   fatjet * Zcand = 0;
   fatjet * leading_lrj = 0;
   fatjet * sub_leading_lrj = 0;

   //fatjet * Selected = 0;
  
   double highestPt = 0;
   double secondPt = 0;
   ////////////////////////////////////////////////////////////////////////
   if(nlargeRjets > 1) {
     cutflow_Vqq->Fill(-0.5,totalWeight);
     cutflow_Vbb->Fill(-0.5,totalWeight);
     //only process events with at least 2 largeRjets;
     // the event is selected;
     ////////////////////////////////////////////////////////////////////////////
   // a Selected has the highest pt. Is it what i want?
  highestPt = 450;
  // find leading lrj, no matter is selected or not.
  for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
     fatjet & largeR = (*lrj);
     if (largeR.isCand() && largeR.Pt() > highestPt) {
       leading_lrj = &largeR;
       highestPt = leading_lrj->Pt();
     } //close largeR.Pt() > highestPt
     else continue;
  } //close for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj)
  //Selected_leading_largeRjet_pt->Fill(leading_lrj->Pt(),totalWeight);
  //Selected_leading_largeRjet_mass->Fill(leading_lrj->M(),totalWeight);

   //std::cout << "highest Pt: " << leadinglrj->Pt() << "MeV" << std::endl;
  // find sub-leading lrj
  //secondPt = 0;
  for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj) {
    fatjet & largeR = (*lrj);
    if (largeR.isCand() && largeR.Pt() < highestPt) {
      if (largeR.Pt() > secondPt) {
	sub_leading_lrj = &largeR;
	secondPt = sub_leading_lrj->Pt();
      } // Close if (largeR.Pt() > secondPt)
      else continue;
   } // Close if (largeR.Pt() < highestPt)
   else continue;
 }// Close for (auto lrj = largeRjets.begin();lrj != largeRjets.end();++lrj)
     //std::cout << "second Pt: " << sublrj->Pt() << "MeV" << std::endl;
  if(leading_lrj != 0 && sub_leading_lrj != 0 ){
    //if(leading_lrj->Pt()>450 && leading_lrj->Pt() > 2.0*leading_lrj->M()){
    cutflow_Vqq->Fill(0.5,totalWeight);
    cutflow_Vbb->Fill(0.5,totalWeight);
    Selected_leading_largeRjet_pt->Fill(leading_lrj->Pt(),totalWeight);
    Selected_leading_largeRjet_mass->Fill(leading_lrj->M(),totalWeight);
    Selected_leading_largeRjet_mass_vs_pt->Fill(leading_lrj->Pt(),leading_lrj->M(),totalWeight);
    //}
  double lrj_deltaR = leading_lrj->DeltaR(*sub_leading_lrj);
  Selected_subleading_largeRjet_pt->Fill(sub_leading_lrj->Pt(),totalWeight);
  Selected_subleading_largeRjet_mass->Fill(sub_leading_lrj->M(),totalWeight);
  Selected_largeRjet_deltaR->Fill(lrj_deltaR,totalWeight); 
  double symmetry = (leading_lrj->Pt() - sub_leading_lrj->Pt())/(leading_lrj->Pt() + sub_leading_lrj->Pt());
  
  if( symmetry < 0.15){ //pT symmetry
    cutflow_Vqq->Fill(1.5,totalWeight);
    cutflow_Vbb->Fill(1.5,totalWeight);
    Selected_pTsymmetry_leading_largeRjet_pt->Fill(leading_lrj->Pt(),totalWeight);
    Selected_pTsymmetry_leading_largeRjet_mass->Fill(leading_lrj->M(),totalWeight);
    Selected_pTsymmetry_subleading_largeRjet_pt->Fill(sub_leading_lrj->Pt(),totalWeight);
    Selected_pTsymmetry_subleading_largeRjet_mass->Fill(sub_leading_lrj->M(),totalWeight);
    Selected_pTsymmetry_largeRjet_deltaR->Fill(lrj_deltaR,totalWeight);
    
    double etaCut = abs(leading_lrj->Eta() - sub_leading_lrj->Eta());
    if( etaCut < 1.2){ //eta cut
      cutflow_Vqq->Fill(2.5,totalWeight);
      cutflow_Vbb->Fill(2.5,totalWeight);
      Selected_pTsymmetry_etaCut_leading_largeRjet_pt->Fill(leading_lrj->Pt(),totalWeight);
      Selected_pTsymmetry_etaCut_leading_largeRjet_mass->Fill(leading_lrj->M(),totalWeight);
      Selected_pTsymmetry_etaCut_subleading_largeRjet_pt->Fill(sub_leading_lrj->Pt(),totalWeight);
      Selected_pTsymmetry_etaCut_subleading_largeRjet_mass->Fill(sub_leading_lrj->M(),totalWeight);
      Selected_pTsymmetry_etaCut_largeRjet_deltaR->Fill(lrj_deltaR,totalWeight);
    

  //the tagged jet could be leading large-R jet of sub-leading large-R jet (eventually third-leading large-R jet)
  // At first time, just try leading large-R jet of sub-leading large-R jet
  // if both are tagged, just take leading large-R jet as tagged jet.

  if( leading_lrj->isTagged && !sub_leading_lrj->isPassD2) { 
    Tagged = leading_lrj;
      //std::cout<<"tagged on leading large-R jet"<< std::endl;
   }
   else if(sub_leading_lrj->isTagged && !leading_lrj->isPassD2 ) {
     Tagged = sub_leading_lrj;
     //std::cout<<"tagged on sub-leading large-R jet"<< std::endl;
   }
   
  if( Tagged !=0 && WqqSamples) {
     //std::cout << "We have a tagged Wqq jet" <<std::endl;
   Wtagged_smooth80_mass->Fill(Tagged->M(), totalWeight);
   Wtagged_smooth80_pt->Fill(Tagged->Pt(), totalWeight);
   }
   if( Tagged !=0 && ZqqSamples) {
     //std::cout << "We have a tagged Zqq jet" <<std::endl;
     Ztagged_smooth80_mass->Fill(Tagged->M(), totalWeight);
     Ztagged_smooth80_pt->Fill(Tagged->Pt(), totalWeight);
   }
   /*if( Tagged !=0 ) {
     //std::cout << "We have a tagged Zqq jet" <<std::endl;
     cutflow->Fill(3.5,totalWeight);
     Tagged_mass->Fill(Tagged->M(), totalWeight);
     Tagged_pt->Fill(Tagged->Pt(), totalWeight);
     }*/

   
   if (Tagged != 0) { 
     //std::cout<<"We have a Selected"<<std::endl;
     Tagged_ntrk->Fill(Tagged->GetNumTrackVectors(),totalWeight);
     trackjet * leadingtrk = 0;
     trackjet * secondtrk = 0;
     double highestPt = 0;
     double secondPt = 0;
     //std::cout<<"NumTrackVectors: " << Selected->GetNumTrackVectors() <<std::endl;
     if(Tagged->GetNumTrackVectors() > 1){
     for (int i=0;i<Tagged->GetNumTrackVectors();++i) {
       trackjet * trk = Tagged->GetTrackVector(i);
       if (trk->Pt() > highestPt) {
	 secondPt = highestPt;
	 secondtrk = leadingtrk;
	 leadingtrk = trk;
	 highestPt = trk->Pt();
       } else if (trk->Pt() > secondPt) {
	 secondtrk = trk;
	 secondPt = trk->Pt();
       } else continue;
       }

   double deltaR = leadingtrk->DeltaR(*secondtrk);
   cutflow_Vqq->Fill(3.5,totalWeight);
   Tagged_mass->Fill(Tagged->M(),totalWeight);
   Tagged_pt->Fill(Tagged->Pt(),totalWeight);
   Tagged_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
   Tagged_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
   Tagged_deltaR->Fill(deltaR,totalWeight);
     }//close at least 2 track jets
      
   } //end eta cut
   //} //end if(leading_lrj != 0 && sub_leading_lrj !=0), we have at least two large radius jet that is Cand. 

   ////////////
   if(leading_lrj->isCand() && leading_lrj->GetNumTrackVectors() > 1){
   if(leading_lrj->M() > 50){
       if(leading_lrj->Pt() > 450){
	 Zcand = leading_lrj;
       }
   } //Get a Z candidate
   }
   if (Zcand != 0) { // Definitely got a Zcand and it has highest pT
     cutflow_Vbb->Fill(3.5,totalWeight);
     Zcand_mass->Fill(Zcand->M(),totalWeight);
     Zcand_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_ntrk->Fill(Zcand->GetNumTrackVectors(),totalWeight);
     trackjet * Zcand_leadingtrk = 0;
     trackjet * Zcand_secondtrk = 0;
     double Zcand_highestPt = 0;
     double Zcand_secondPt = 0;
     //std::cout<<"NumTrackVectors: " << Selected->GetNumTrackVectors() <<std::endl;
     for (int i=0;i<Zcand->GetNumTrackVectors();++i) {
       trackjet * Zcand_trk = Zcand->GetTrackVector(i);
       if (Zcand_trk->Pt() > Zcand_highestPt) {
         Zcand_secondPt = Zcand_highestPt;
         Zcand_secondtrk = Zcand_leadingtrk;
         Zcand_leadingtrk = Zcand_trk;
         Zcand_highestPt = Zcand_trk->Pt();
       } else if (Zcand_trk->Pt() > Zcand_secondPt) {
         Zcand_secondtrk = Zcand_trk;
         Zcand_secondPt = Zcand_trk->Pt();
       } else continue;
     }
     double Zcand_deltaR = Zcand_leadingtrk->DeltaR(*Zcand_secondtrk);
   if (Zcand->GetBStatus() == 1) {
     //      std::cout<<"0 btags"<<std::endl;
     cutflow_Vbb->Fill(4.5,totalWeight);
     Zcand_0b_MV2_mass->Fill(Zcand->M(),totalWeight);
     Zcand_0b_MV2_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_0b_MV2_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_0b_MV2_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_0b_MV2_deltaR->Fill(Zcand_deltaR,totalWeight);
   } // Close (Zcand->GetBStatus() == 1)
   else if (Zcand->GetBStatus() == 2) {
     //      std::cout<<"1 btag"<<std::endl;
     cutflow_Vbb->Fill(7.5,totalWeight);
     Zcand_1b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);
   } // Close if (Zcand->GetBStatus() == 2)

   // Find Zcand with 1 btag MV2 77% WP
   else if (Zcand->GetBStatus() == 3) {
     //      std::cout<<"1 btag"<<std::endl;
     /*Tagged_1b_MV2_77_mass->Fill(Tagged->M(),totalWeight);
     Tagged_1b_MV2_77_pt->Fill(Tagged->Pt(),totalWeight);
     Tagged_1b_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Tagged_1b_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Tagged_1b_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(7.5,totalWeight);
     Zcand_1b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);
   } // Close if (Zcand->GetBStatus() == 3)

   // Find Zcand with 1 btag MV2 70% WP
   else if (Zcand->GetBStatus() == 4) {
     //      std::cout<<"1 btag"<<std::endl;
     cutflow_Vbb->Fill(5.5,totalWeight);
     Zcand_1b_MV2_70_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_70_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_70_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_70_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_70_deltaR->Fill(Zcand_deltaR,totalWeight);
     /*Zcand_1b_MV2_77_mass->Fill(Tagged->M(),totalWeight);
     Zcand_1b_MV2_77_pt->Fill(Tagged->Pt(),totalWeight);
     Tagged_1b_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Tagged_1b_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Tagged_1b_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(7.5,totalWeight);
     Zcand_1b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);
   } // Close if (Zcand->GetBStatus() == 4)
   else if (Zcand->GetBStatus() == 5) {
     //      std::cout<<"1 btag"<<std::endl;
     /* Tagged_1b_MV2_60_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_60_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_60_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_60_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_60_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(5.5,totalWeight);
     Zcand_1b_MV2_70_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_70_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_70_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_70_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_70_deltaR->Fill(Zcand_deltaR,totalWeight);     
     /*Zcand_1b_MV2_77_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_77_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(7.5,totalWeight);
     Zcand_1b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_1b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_1b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_1b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);     
   } // Close if (Zcand->GetBStatus() == 5)
   
   // Find Zcand with 2 btags at 85% WP
   else if (Zcand->GetBStatus() == 6) {
     //    std::cout<<"2 btag"<<std::endl;
     cutflow_Vbb->Fill(8.5,totalWeight);
     Zcand_2b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);    

   } // Close if (Zcand->GetBStatus() == 6) 

   else if (Zcand->GetBStatus() == 7) {
     //    std::cout<<"2 btag"<<std::endl;
     /*Zcand_sr_MV2_77_mass->Fill(Zcand->M(),totalWeight);
     Zcand_sr_MV2_77_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_sr_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(8.5,totalWeight);
     Zcand_2b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);

   } // Close if (Zcand->GetBStatus() == 7)    

   // Find Zcand with 2 btags at 70% WP
   else if (Zcand->GetBStatus() == 8) {
     //    std::cout<<"2 btag"<<std::endl;
     cutflow_Vbb->Fill(6.5,totalWeight);
     Zcand_2b_MV2_70_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_70_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_70_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_70_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_70_deltaR->Fill(Zcand_deltaR,totalWeight);
     /*Zcand_sr_MV2_77_M->Fill(Zcand->M(),totalWeight);
     Zcand_sr_MV2_77_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_sr_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(8.5,totalWeight);
     Zcand_2b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);
   } // Close if (Zcand->GetBStatus() == 8)

   else if (Zcand->GetBStatus() == 9) {
     //    std::cout<<"2 btag"<<std::endl;
     /*Zcand_sr_MV2_60_mass->Fill(Zcand->M(),totalWeight);
     Zcand_sr_MV2_60_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_sr_MV2_60_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_sr_MV2_60_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_sr_MV2_60_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(6.5,totalWeight);
     Zcand_2b_MV2_70_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_70_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_70_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_70_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_70_deltaR->Fill(Zcand_deltaR,totalWeight);
     /*Zcand_sr_MV2_77_M->Fill(Zcand->M(),totalWeight);
     Zcand_sr_MV2_77_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_sr_MV2_77_leadtrk_pt->Fill(leadingtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_subtrk_pt->Fill(secondtrk->Pt(),totalWeight);
     Zcand_sr_MV2_77_deltaR->Fill(deltaR,totalWeight);*/
     cutflow_Vbb->Fill(8.5,totalWeight);
     Zcand_2b_MV2_85_mass->Fill(Zcand->M(),totalWeight);
     Zcand_2b_MV2_85_pt->Fill(Zcand->Pt(),totalWeight);
     Zcand_2b_MV2_85_leadtrk_pt->Fill(Zcand_leadingtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_subtrk_pt->Fill(Zcand_secondtrk->Pt(),totalWeight);
     Zcand_2b_MV2_85_deltaR->Fill(Zcand_deltaR,totalWeight);     

   } // Close if (Zcand->GetBStatus() == 9)
   } // Close if (Zcand != 0)
   ////////////////////////////////////////////////////
   //end Vbb
   
    } // Close if (Pt symmetry)
  }// Close if (Eta cut)
   // std::cout<<"End of Process"<<std::endl;
  }//Close if leading !=0 && sub_leading !=0 
   ///////////////////////////////////////////////////////////////////////////////
   } //Close if (nlargeRjet > 1)
   /////////////////////////////////////////////////////////////////////////////
   fReader.SetEntry(entry);
   return kTRUE;
} // Close Nominal

void Nominal::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}

void Nominal::Terminate(TH1D * h_stat[16],TString mc_filename)
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.

  TFile *f = new TFile(mc_filename+".root","RECREATE");

  if (h_stat[0]) {
    for (int i=0;i<16;++i) {
      h_stat[i]->Write();
    }
  }
  
  for (uint i=0;i<histlist.size();++i) {
    (histlist.at(i))->Write();
    delete histlist.at(i);
  }
  for (uint i=0;i<histlist2.size();++i) {
    (histlist2.at(i))->Write();
    delete histlist2.at(i);
  }
  f->Close();

}

TH1D * Nominal::makeHist(TString name,int nbins,double x1, double x2) {
  TH1D * hist = new TH1D(name,name,nbins,x1,x2);
  hist->Sumw2();
  histlist.push_back(hist);
  return hist;
}

TH2D * Nominal::makeHist2(TString name,int xnbins,double x1, double x2, int ynbins, double y1, double y2) {
  TH2D * hist2 = new TH2D(name,name,xnbins,x1,x2,ynbins,y1,y2);
  hist2->Sumw2();
  histlist2.push_back(hist2);
  return hist2;
}

void Nominal::FatToTrackLinks(std::vector<fatjet> & largeRjets,std::vector<trackjet> & trackjets,int & nlargeRjets){
  //  std::cout<<"Creating links between fatjet and trackjets."<<std::endl;
  for (int i=0;i<nlargeRjets;++i) {
    //    std::cout<<"i= "<<i<<std::endl;
    fatjet & fj = largeRjets[i];
    //    std::cout<<"We have largeRjet "<<i<<std::endl;
    std::vector<unsigned long> tjvector = LargeRJetAssociatedTrackJet[i];
    //    std::cout<<"We have the vector of trackjets."<<std::endl;

    for (auto tj=tjvector.begin();tj!=tjvector.end();++tj) { 
      trackjet & tjassociated = trackjets[*tj];
      if (TrackJetIsSelected[*tj] == false) continue;
      else if (TrackJetPassDR[*tj] == false) continue;
      (fj).AddTrackJetPointer(&(tjassociated));
    }
  }
  //  std::cout<<"Exiting for loop."<<std::endl;
}

void Nominal::EraseUnselectedFatjets(std::vector<fatjet> & largeRjets) {
  for (uint i=0;i<largeRjets.size();++i) {
    if (LargeRJetIsSelected[i] == false) {
      largeRjets.erase(largeRjets.begin()+i);
    }
    else continue;
  }
}
    
std::pair<std::map<std::string, double>, bool> GetMCWeightMap() {
  std::map<std::string, double> MCWeightMap;
  bool Success;
  PyObject *pName;
  PyObject *pModule;
  PyObject *pFunc;

  PyObject *pDict;

  Py_Initialize();
  PyRun_SimpleString("import sys");
  PyRun_SimpleString("sys.path.append(\".\")");
  pName = PyString_FromString("read_json");
  pModule = PyImport_Import(pName);
  Py_DECREF(pName);

  if (pModule != nullptr) {
    pFunc = PyObject_GetAttrString(pModule,"read_file");

    if (pFunc && PyCallable_Check(pFunc)) {
      pDict = PyObject_CallObject(pFunc, nullptr);
      if (pDict != nullptr) {
	PyObject *pKeys = PyDict_Keys(pDict);
	PyObject *pValues = PyDict_Values(pDict);
	for (Py_ssize_t i = 0; i < PyDict_Size(pDict); ++i) { 
	  auto pair = std::make_pair(std::string(PyString_AsString(PyList_GetItem(pKeys, i))), PyFloat_AsDouble(PyList_GetItem(pValues, i)));
	  MCWeightMap.insert( pair );
	}
	Success = true;
	Py_DECREF(pDict);
      }
      else {
	Py_DECREF(pFunc);
	Py_DECREF(pModule);
	PyErr_Print();
	fprintf(stderr,"Call failed\n");
	Success = false;
	return std::make_pair(MCWeightMap,Success);
      }
    }
    else {
      if (PyErr_Occurred())
	PyErr_Print();
      fprintf(stderr,"Cannot find function \"%s\"\n", "read_file");
    }
    Py_XDECREF(pFunc);
    Py_DECREF(pModule);
  }
  else {
    PyErr_Print();
    fprintf(stderr, "Failed to load \"%s\"\n", "read_json");
    Success = false;
    return std::make_pair(MCWeightMap,Success);
  }
  Py_Finalize();
  Success = true;
  for (auto const& pair: MCWeightMap) {
     std::cout<<"{" <<(pair.first) << ":"<<(pair.second)<<"}"<<std::endl;
   }

  return std::make_pair(MCWeightMap,Success);
}	    

std::vector<TString> list_files(const char *dirname="C:/root/folder/", const char *ext=".root") {
  std::vector<TString> result;
  TSystemDirectory dir(dirname, dirname);
  TList *files = dir.GetListOfFiles();
  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (!file->IsDirectory() && fname.EndsWith(ext)) {
	std::cout << "list_files: " <<fname.Data() << std::endl;
	result.push_back(fname);
      }
    }
  }
  return result;
}

void dogen(TString path,TString fname) {
  Nominal * selector = new Nominal();
  TChain * chain = new TChain ("nominal");
  TChain * chain_2 = new TChain("sumWeights");
  
  std::vector<TString> list = list_files(path);

  TH1D * h_stat[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

  std::cout<<"dogen: Found "<<list.size()<<" files"<<std::endl;
  if (list.size() == 0) return;
  for (uint i=0;i<list.size();++i) {
    std::cout<<"dogen: Adding "<<path+list[i]<<std::endl;
    chain->Add(path+list[i]);
    chain_2->Add(path+list[i]);
    TFile * inputfile = TFile::Open(path+list[i]);
    inputfile->cd("Analysis");
    TString statnames[16] = {"cutflow","cutflow_mc","cutflow_pu","cutflow_zvtx","cutflow_mc_pu","cutflow_mc_pu_zvtx","cutflow_scale_factors","cutflow_btag_scale_factors","cutflow_Loose","cutflow_mc_Loose","cutflow_pu_Loose","cutflow_zvtx_Loose","cutflow_mc_pu_Loose","cutflow_mc_pu_zvtx_Loose","cutflow_scale_factors_Loose","cutflow_btag_scale_factors_Loose"};
    for (int j=0;j<16;++j) {
      TH1D * temp = (TH1D*)gROOT->FindObject(statnames[j]);
      std::cout<<"Address of " <<statnames[j]<<" is "<<temp<<std::endl;
      if (h_stat[j] == 0) {
	std::cout<<"Store "<<statnames[j]<<" in h_stat["<<j<<"] "<<std::endl;
	h_stat[j] = temp;
	if (h_stat[j] != 0) h_stat[j]->SetDirectory(0);
      } else {
	std::cout<<"Add "<<statnames[j]<<" to h_stat["<<j<<"] "<<std::endl;
	if (temp != 0) h_stat[j]->Add(temp);
      }
      }
    inputfile->Close();
  }

  chain->Lookup(true);
  chain_2->Lookup(true);
  //TString mc_filename = "mc16_13TeV.304307.Sherpa_CT10_Wqq_Pt280_500.deriv.DAOD_JETM6.e4692_s3126_r10201_p3916"; 
  TString mc_filename = path;
  //TString toErase = "/data/atlas/yhe/QT/Vjets-ntuple/user.yajun";
  //TString toErase2 = ".Sherpa225.WZtag80.test_LCtopo-output.root/";
  //TString toErase = "/home/yhe/QT/eleanor/qualification_task/WqqTest/"; 
  //Ssiz_t pos = mc_filename.Index(toErase);
  //if (pos-1 != mc_filename.Length()) mc_filename.Remove(pos, toErase.Length());
  //Ssiz_t pos2 = mc_filename.Index(toErase2);
  //if (pos2-1 != mc_filename.Length()) mc_filename.Remove(pos2, toErase2.Length());
  //mc_filename.Insert(0, "mc16_13TeV.");
  //Ssiz_t pos3 = mc_filename.Index("Sherpa")
  //Ssiz_t pos3 = mc_filename.Last('e');
  //mc_filename.Insert(pos3, "deriv.DAOD_JETM4.");*/
  if(mc_filename.Contains("r9364")){ //mc16a
    if (mc_filename.Contains("364375")) mc_filename = "mc16_13TeV.364375.Sherpa_225_NNPDF30NNLO_Zqq_PTV280_500.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364376")) mc_filename = "mc16_13TeV.364376.Sherpa_225_NNPDF30NNLO_Zqq_PTV500_1000.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364377")) mc_filename = "mc16_13TeV.364377.Sherpa_225_NNPDF30NNLO_Zqq_PTV1000_E_CMS.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364378")) mc_filename = "mc16_13TeV.364378.Sherpa_225_NNPDF30NNLO_Wqq_PTV280_500.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364379")) mc_filename = "mc16_13TeV.364379.Sherpa_225_NNPDF30NNLO_Wqq_PTV500_1000.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364380")) mc_filename = "mc16_13TeV.364380.Sherpa_225_NNPDF30NNLO_Wqq_PTV1000_E_CMS.deriv.DAOD_JETM6.e7048_s3126_r9364_p3916";
    if (mc_filename.Contains("364700")) mc_filename = "mc16_13TeV.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364701"))mc_filename = "mc16_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364702"))mc_filename = "mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364703"))mc_filename = "mc16_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364704"))mc_filename = "mc16_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364705"))mc_filename = "mc16_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364706"))mc_filename = "mc16_13TeV.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364707"))mc_filename = "mc16_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364708"))mc_filename = "mc16_13TeV.364708.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364709"))mc_filename = "mc16_13TeV.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364710"))mc_filename = "mc16_13TeV.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364711"))mc_filename = "mc16_13TeV.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";
    if (mc_filename.Contains("364712"))mc_filename = "mc16_13TeV.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.deriv.DAOD_JETM6.e7142_s3126_r9364_p3916";  
    if (mc_filename.Contains("410285"))mc_filename = "mc16_13TeV.410285.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1300_1500.deriv.DAOD_JETM6.e6686_s3126_r9364_p3916";
    if (mc_filename.Contains("410286"))mc_filename = "mc16_13TeV.410286.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1500_1700.deriv.DAOD_JETM6.e6686_s3126_r9364_p3916";
    if (mc_filename.Contains("410287"))mc_filename = "mc16_13TeV.410287.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1700_2000.deriv.DAOD_JETM6.e6686_s3126_r9364_p3916";
    if (mc_filename.Contains("410288"))mc_filename = "mc16_13TeV.410288.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_2000_14000.deriv.DAOD_JETM6.e6686_s3126_r9364_p3916";
  }
  if(mc_filename.Contains("r10201")){ //mc16d
    if (mc_filename.Contains("364375")) mc_filename = "mc16_13TeV.364375.Sherpa_225_NNPDF30NNLO_Zqq_PTV280_500.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364376")) mc_filename = "mc16_13TeV.364376.Sherpa_225_NNPDF30NNLO_Zqq_PTV500_1000.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364377")) mc_filename = "mc16_13TeV.364377.Sherpa_225_NNPDF30NNLO_Zqq_PTV1000_E_CMS.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364378")) mc_filename = "mc16_13TeV.364378.Sherpa_225_NNPDF30NNLO_Wqq_PTV280_500.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364379")) mc_filename = "mc16_13TeV.364379.Sherpa_225_NNPDF30NNLO_Wqq_PTV500_1000.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364380")) mc_filename = "mc16_13TeV.364380.Sherpa_225_NNPDF30NNLO_Wqq_PTV1000_E_CMS.deriv.DAOD_JETM6.e7048_s3126_r10201_p3916";
    if (mc_filename.Contains("364700"))mc_filename = "mc16_13TeV.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364701"))mc_filename = "mc16_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364702"))mc_filename = "mc16_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364703"))mc_filename = "mc16_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364704"))mc_filename = "mc16_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364705"))mc_filename = "mc16_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364706"))mc_filename = "mc16_13TeV.364706.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364707"))mc_filename = "mc16_13TeV.364707.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364708"))mc_filename = "mc16_13TeV.364708.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364709"))mc_filename = "mc16_13TeV.364709.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364710"))mc_filename = "mc16_13TeV.364710.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364711"))mc_filename = "mc16_13TeV.364711.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("364712"))mc_filename = "mc16_13TeV.364712.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12WithSW.deriv.DAOD_JETM6.e7142_s3126_r10201_p3916";
    if (mc_filename.Contains("410285"))mc_filename = "mc16_13TeV.410285.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1300_1500.deriv.DAOD_JETM6.e6686_s3126_r10201_p3916";
    if (mc_filename.Contains("410286"))mc_filename = "mc16_13TeV.410286.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1500_1700.deriv.DAOD_JETM6.e6686_s3126_r10201_p3916";
    if (mc_filename.Contains("410287"))mc_filename = "mc16_13TeV.410287.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_1700_2000.deriv.DAOD_JETM6.e6686_s3126_r10201_p3916";
    if (mc_filename.Contains("410288"))mc_filename = "mc16_13TeV.410288.PhPy8EG_A14_ttbar_hdamp258p75_allhad_mtt_2000_14000.deriv.DAOD_JETM6.e6686_s3126_r10201_p3916";
  }
  
  std::map<std::string, double> weight_map = (GetMCWeightMap()).first;
  for (auto const& pair: weight_map) {
     std::cout<<"{" <<(pair.first) << ":"<<(pair.second)<<"}"<<std::endl;
   }
  for (auto iter = weight_map.begin(); iter != weight_map.end(); ++iter) {
    if (iter->first == mc_filename) {
      selector->mc_weighted = iter->second;
    }
  }
  std::cout<<mc_filename<< " : "<< "mc_weighted"<< " : " << selector->mc_weighted<<std::endl;

  TTreeReader sumWeightsReader;
  sumWeightsReader.SetTree(chain_2);
  TTreeReaderValue<Float_t> totalEventsWeighted = {sumWeightsReader,"totalEventsWeighted"};

  selector->Init(chain);
  selector->Begin(0);
  selector->Notify();

  std::cout<<"dogen: Will use "<<chain->GetEntries()<<" entries"<<std::endl;

  for (int i=0; i<chain_2->GetEntries();++i) {
    sumWeightsReader.SetEntry(i);
    selector->dsid_weights+=*totalEventsWeighted;
   }
  std::cout<<"dsid_weights : " << selector->dsid_weights<<std::endl;
  for (int i=0;i<chain->GetEntries();++i) {
    selector->Process(i);
    //std::cout<<"Process "<<i<<"finished"<<std::endl;
  }
  std::cout<<"Process finished"<<std::endl;
  selector->Terminate(h_stat,mc_filename);

  std::cout<<"Will execute Exec(mv mc_filename.root "<<fname<<")";
  gSystem->Exec("mv "+mc_filename+".root "+fname);
  selector->Delete();
}

int main(int argc, char **argv) {
  if (argc < 4) {
    std::cout<<"main called with only "<<argc<<" arguments; quit"<<std::endl;
    return 1;
  }

  TString opdir = argv[1];
  TString infile = argv[2];
  TString outfile = argv[3];
  TString base = "/data/atlas/phrsbc/qualification_task_set2/";
  if (argc > 4) base = argv[4];

  std::cout<<"Nominal: opdir "<<opdir<<std::endl;
  std::cout<<"Nominal: infile "<<infile<<std::endl;
  std::cout<<"Nominal: outfile "<<outfile<<std::endl;
  std::cout<<"Nominal: base "<<base<<std::endl;

  if (!gSystem->Exec("ls "+opdir)) {
    std::cout<<"pathname " <<opdir<<" exists"<<std::endl;
  } else {
    std::cout<<"pathname "<<opdir<<" does not exist. It will be made"<<std::endl;
    gSystem->Exec("mkdir "+opdir);
  }
  dogen(base+infile+"/",opdir+"/"+outfile);
  return 0;
}
 
