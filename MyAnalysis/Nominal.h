//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Apr  3 17:39:23 2019 by ROOT version 6.14/08
// from TTree nominal/tree
// found on file: output-zgamma1.root
//////////////////////////////////////////////////////////

#ifndef Nominal_h
#define Nominal_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "fatjet.h"

// Headers needed by this particular selector
#include <vector>
#include <string>
#include <map>


class Nominal : public TSelector {
public :
   TTreeReader    fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<uint32_t> DSID = {fReader, "DSID"};
   //TTreeReaderValue<uint32_t> RunNumber = {fReader, "RunNumber"};
   TTreeReaderValue<unsigned long long> EventNumber = {fReader, "EventNumber"};
   TTreeReaderValue<uint32_t> LumiBlock = {fReader, "LumiBlock"};
   TTreeReaderValue<Float_t> EventWeight = {fReader, "EventWeight"};
   TTreeReaderArray<char> JetIsSelected = {fReader, "JetIsSelected"};
   TTreeReaderArray<char> JetIsSelectedLoose = {fReader, "JetIsSelectedLoose"};
   TTreeReaderArray<float> JetPt = {fReader, "JetPt"};
   TTreeReaderArray<float> JetEta = {fReader, "JetEta"};
   TTreeReaderArray<float> JetPhi = {fReader, "JetPhi"};
   TTreeReaderArray<float> JetMass = {fReader, "JetMass"};
   TTreeReaderArray<char> TrackJetIsSelected = {fReader, "TrackJetIsSelected"};
   TTreeReaderArray<float> TrackJetPt = {fReader, "TrackJetPt"};
   TTreeReaderArray<float> TrackJetEta = {fReader, "TrackJetEta"};
   TTreeReaderArray<float> TrackJetPhi = {fReader, "TrackJetPhi"};
   TTreeReaderArray<float> TrackJetMass = {fReader, "TrackJetMass"};
   TTreeReaderArray<char> TrackJetPassDR = {fReader, "TrackJetPassDR"};
   TTreeReaderArray<char> ElectronIsSelected = {fReader, "ElectronIsSelected"};
   TTreeReaderArray<char> ElectronIsSelectedLoose = {fReader, "ElectronIsSelectedLoose"};
   TTreeReaderArray<float> ElectronPt = {fReader, "ElectronPt"};
   TTreeReaderArray<float> ElectronEta = {fReader, "ElectronEta"};
   TTreeReaderArray<float> ElectronPhi = {fReader, "ElectronPhi"};
   //TTreeReaderArray<char> PhotonIsSelected = {fReader, "PhotonIsSelected"};
   //TTreeReaderArray<char> PhotonIsSelectedLoose = {fReader, "PhotonIsSelectedLoose"};
   //TTreeReaderArray<float> PhotonPt = {fReader, "PhotonPt"};
   //TTreeReaderArray<float> PhotonEta = {fReader, "PhotonEta"};
   //TTreeReaderArray<float> PhotonPhi = {fReader, "PhotonPhi"};
   TTreeReaderArray<char> MuonIsSelected = {fReader, "MuonIsSelected"};
   TTreeReaderArray<char> MuonIsSelectedLoose = {fReader, "MuonIsSelectedLoose"};
   TTreeReaderArray<float> MuonPt = {fReader, "MuonPt"};
   TTreeReaderArray<float> MuonEta = {fReader, "MuonEta"};
   TTreeReaderArray<float> MuonPhi = {fReader, "MuonPhi"};
   TTreeReaderArray<char> LargeRJetIsSelected = {fReader, "LargeRJetIsSelected"};
   TTreeReaderArray<float> LargeRJetPt = {fReader, "LargeRJetPt"};
   TTreeReaderArray<float> LargeRJetEta = {fReader, "LargeRJetEta"};
   TTreeReaderArray<float> LargeRJetPhi = {fReader, "LargeRJetPhi"};
   TTreeReaderArray<float> LargeRJetMass = {fReader, "LargeRJetMass"};
   TTreeReaderArray<float> LargeRJetXbbScoreTop = {fReader, "LargeRJetXbbScoreTop"};
   TTreeReaderArray<float> LargeRJetXbbScoreQCD = {fReader, "LargeRJetXbbScoreQCD"};
   TTreeReaderArray<float> LargeRJetXbbScoreHiggs = {fReader, "LargeRJetXbbScoreHiggs"};
   
   TTreeReaderArray<int> LargeRJetPassWD2 = {fReader, "LargeRJetPassWD2"};
   TTreeReaderArray<int> LargeRJetPassWNtrk = {fReader, "LargeRJetPassWNtrk"};
   TTreeReaderArray<int> LargeRJetPassWMassHigh = {fReader, "LargeRJetPassWMassHigh"};
   TTreeReaderArray<int> LargeRJetPassWMassLow = {fReader, "LargeRJetPassWMassLow"};
   //TTreeReaderArray<int> LargeRJetPassWNtrkD2 = {fReader, "LargeRJetPassWNtrkD2"};
   TTreeReaderArray<int> LargeRJetPassZD2 = {fReader, "LargeRJetPassZD2"};
   TTreeReaderArray<int> LargeRJetPassZNtrk = {fReader, "LargeRJetPassZNtrk"};
   TTreeReaderArray<int> LargeRJetPassZMassHigh= {fReader, "LargeRJetPassZMassHigh"};
   TTreeReaderArray<int> LargeRJetPassZMassLow = {fReader, "LargeRJetPassZMassLow"};
   
   TTreeReaderArray<std::vector<unsigned long>> LargeRJetAssociatedTrackJet = {fReader, "LargeRJetAssociatedTrackJet"};
   TTreeReaderValue<Bool_t> Analysis = {fReader, "Analysis"};
   TTreeReaderArray<char> JetIsBTagged_DL1rmu_FixedCutBEff_60 = {fReader, "JetIsBTagged_DL1rmu_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_MV2c10_FixedCutBEff_60 = {fReader, "JetIsBTagged_MV2c10_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_MV2c10_FixedCutBEff_70 = {fReader, "JetIsBTagged_MV2c10_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_MV2c10_FixedCutBEff_77 = {fReader, "JetIsBTagged_MV2c10_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_MV2c10_FixedCutBEff_85 = {fReader, "JetIsBTagged_MV2c10_FixedCutBEff_85"};
   TTreeReaderArray<char> JetIsBTagged_MV2rmu_FixedCutBEff_60 = {fReader, "JetIsBTagged_MV2rmu_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_MV2rmu_FixedCutBEff_70 = {fReader, "JetIsBTagged_MV2rmu_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_MV2rmu_FixedCutBEff_77 = {fReader, "JetIsBTagged_MV2rmu_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_MV2rmu_FixedCutBEff_85 = {fReader, "JetIsBTagged_MV2rmu_FixedCutBEff_85"};
   TTreeReaderArray<char> JetIsBTagged_MV2r_FixedCutBEff_60 = {fReader, "JetIsBTagged_MV2r_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_MV2r_FixedCutBEff_70 = {fReader, "JetIsBTagged_MV2r_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_MV2r_FixedCutBEff_77 = {fReader, "JetIsBTagged_MV2r_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_MV2r_FixedCutBEff_85 = {fReader, "JetIsBTagged_MV2r_FixedCutBEff_85"};
   TTreeReaderArray<char> JetIsBTagged_DL1_FixedCutBEff_60 = {fReader, "JetIsBTagged_DL1_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_DL1_FixedCutBEff_70 = {fReader, "JetIsBTagged_DL1_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_DL1_FixedCutBEff_77 = {fReader, "JetIsBTagged_DL1_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_DL1_FixedCutBEff_85 = {fReader, "JetIsBTagged_DL1_FixedCutBEff_85"};
   TTreeReaderArray<char> JetIsBTagged_DL1rmu_FixedCutBEff_70 = {fReader, "JetIsBTagged_DL1rmu_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_DL1rmu_FixedCutBEff_77 = {fReader, "JetIsBTagged_DL1rmu_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_DL1rmu_FixedCutBEff_85 = {fReader, "JetIsBTagged_DL1rmu_FixedCutBEff_85"};
   TTreeReaderArray<char> JetIsBTagged_DL1r_FixedCutBEff_60 = {fReader, "JetIsBTagged_DL1r_FixedCutBEff_60"};
   TTreeReaderArray<char> JetIsBTagged_DL1r_FixedCutBEff_70 = {fReader, "JetIsBTagged_DL1r_FixedCutBEff_70"};
   TTreeReaderArray<char> JetIsBTagged_DL1r_FixedCutBEff_77 = {fReader, "JetIsBTagged_DL1r_FixedCutBEff_77"};
   TTreeReaderArray<char> JetIsBTagged_DL1r_FixedCutBEff_85 = {fReader, "JetIsBTagged_DL1r_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1rmu_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_DL1rmu_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2c10_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_MV2c10_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2c10_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_MV2c10_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2c10_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_MV2c10_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2c10_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_MV2c10_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2rmu_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_MV2rmu_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2rmu_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_MV2rmu_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2rmu_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_MV2rmu_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2rmu_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_MV2rmu_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2r_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_MV2r_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2r_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_MV2r_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2r_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_MV2r_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_MV2r_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_MV2r_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_DL1_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_DL1_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_DL1_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_DL1_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1rmu_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_DL1rmu_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1rmu_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_DL1rmu_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1rmu_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_DL1rmu_FixedCutBEff_85"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_60 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_60"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_70 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_70"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_77 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_77"};
   TTreeReaderArray<char> TrackJetIsBTagged_DL1r_FixedCutBEff_85 = {fReader, "TrackJetIsBTagged_DL1r_FixedCutBEff_85"};
   
   float mc_weighted;
   float dsid_weights;

   // TH1D * selection;
   TH1D * cutflow_Vqq;
   TH1D * cutflow_Vbb;
   TH1D * jet_pt;
   TH1D * jet_mass;
   TH1D * largeRjet_pt;
   TH1D * largeRjet_mass;

   TH1D * trackjet_pt;
   TH1D * trackjet_mass;
   TH1D * muon_pt;
   TH1D * ntrk_hist;

   TH1D * Selected_leading_largeRjet_pt;   
   TH1D * Selected_leading_largeRjet_mass;
   TH1D * Selected_subleading_largeRjet_pt;
   TH1D * Selected_subleading_largeRjet_mass;
   TH1D * Selected_largeRjet_deltaR;

   TH1D * Selected_pTsymmetry_leading_largeRjet_pt;
   TH1D * Selected_pTsymmetry_leading_largeRjet_mass;
   TH1D * Selected_pTsymmetry_subleading_largeRjet_pt;
   TH1D * Selected_pTsymmetry_subleading_largeRjet_mass;
   TH1D * Selected_pTsymmetry_largeRjet_deltaR;

   TH1D * Selected_pTsymmetry_etaCut_leading_largeRjet_pt;
   TH1D * Selected_pTsymmetry_etaCut_leading_largeRjet_mass;
   TH1D * Selected_pTsymmetry_etaCut_subleading_largeRjet_pt;
   TH1D * Selected_pTsymmetry_etaCut_subleading_largeRjet_mass;
   TH1D * Selected_pTsymmetry_etaCut_largeRjet_deltaR;

   TH1D * Wtagged_smooth50_mass;
   TH1D * Wtagged_smooth50_pt;
   TH1D * Wtagged_smooth80_mass;
   TH1D * Wtagged_smooth80_pt;

   TH1D * Ztagged_smooth50_mass;
   TH1D * Ztagged_smooth50_pt;
   TH1D * Ztagged_smooth80_mass;
   TH1D * Ztagged_smooth80_pt;

   TH1D * Tagged_mass;
   TH1D * Tagged_pt;
   TH1D * Tagged_ntrk;
   TH1D * Tagged_leadtrk_pt;
   TH1D * Tagged_subtrk_pt;
   TH1D * Tagged_deltaR;
   
   TH1D * Zcand_ntrk;   
   TH1D * Zcand_mass;
   TH1D * Zcand_pt;
   TH1D * Zcand_leadtrk_pt;
   TH1D * Zcand_subtrk_pt;
   TH1D * Zcand_deltaR;

   TH1D * Zcand_0b_MV2_mass;
   TH1D * Zcand_0b_MV2_pt;
   TH1D * Zcand_0b_MV2_leadtrk_pt;
   TH1D * Zcand_0b_MV2_subtrk_pt;
   TH1D * Zcand_0b_MV2_deltaR;

   TH1D * Zcand_1b_MV2_60_mass;
   TH1D * Zcand_1b_MV2_60_pt;
   TH1D * Zcand_1b_MV2_60_leadtrk_pt;
   TH1D * Zcand_1b_MV2_60_subtrk_pt;
   TH1D * Zcand_1b_MV2_60_deltaR;

   TH1D * Zcand_2b_MV2_60_mass;
   TH1D * Zcand_2b_MV2_60_pt;
   TH1D * Zcand_2b_MV2_60_leadtrk_pt;
   TH1D * Zcand_2b_MV2_60_subtrk_pt;
   TH1D * Zcand_2b_MV2_60_deltaR;

   TH1D * Zcand_1b_MV2_70_mass;
   TH1D * Zcand_1b_MV2_70_pt;
   TH1D * Zcand_1b_MV2_70_leadtrk_pt;
   TH1D * Zcand_1b_MV2_70_subtrk_pt;
   TH1D * Zcand_1b_MV2_70_deltaR;
   
   TH1D * Zcand_2b_MV2_70_mass;
   TH1D * Zcand_2b_MV2_70_pt;
   TH1D * Zcand_2b_MV2_70_leadtrk_pt;
   TH1D * Zcand_2b_MV2_70_subtrk_pt;
   TH1D * Zcand_2b_MV2_70_deltaR;

   TH1D * Zcand_1b_MV2_77_mass;
   TH1D * Zcand_1b_MV2_77_pt;
   TH1D * Zcand_1b_MV2_77_leadtrk_pt;
   TH1D * Zcand_1b_MV2_77_subtrk_pt;
   TH1D * Zcand_1b_MV2_77_deltaR;

   TH1D * Zcand_2b_MV2_77_mass;
   TH1D * Zcand_2b_MV2_77_pt;
   TH1D * Zcand_2b_MV2_77_leadtrk_pt;
   TH1D * Zcand_2b_MV2_77_subtrk_pt;
   TH1D * Zcand_2b_MV2_77_deltaR;

   TH1D * Zcand_1b_MV2_85_mass;
   TH1D * Zcand_1b_MV2_85_pt;
   TH1D * Zcand_1b_MV2_85_leadtrk_pt;
   TH1D * Zcand_1b_MV2_85_subtrk_pt;
   TH1D * Zcand_1b_MV2_85_deltaR;

   TH1D * Zcand_2b_MV2_85_mass;
   TH1D * Zcand_2b_MV2_85_pt;
   TH1D * Zcand_2b_MV2_85_leadtrk_pt;
   TH1D * Zcand_2b_MV2_85_subtrk_pt;
   TH1D * Zcand_2b_MV2_85_deltaR;

   std::vector<TH1*> histlist;
   //TH2D
   TH2D * largeRjet_mass_vs_pt;
   TH2D * Selected_leading_largeRjet_mass_vs_pt;

   std::vector<TH2*> histlist2;

   Nominal(TTree * /*tree*/ =0) { }
   virtual ~Nominal() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate(TH1D * h_stat[16],TString mc_filename);

   TH1D * makeHist(TString name,int nbins,double x1,double x2);
   TH2D * makeHist2(TString name, int xnbins, double x1, double x2, int ynbins, double y1, double y2);
   void FatToTrackLinks(std::vector<fatjet> & largeRjets,std::vector<trackjet> & trackjets, int & nlargeRjets);
   void EraseUnselectedFatjets(std::vector<fatjet> & largeRjets);
   ClassDef(Nominal,0);

};

#endif

#ifdef Nominal_cxx
void Nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   fReader.SetTree(tree);
}

Bool_t Nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef Nominal_cxx
