//////////////////////////////////////////////////////////
// This class has been written by Bill Murray    5th April 2018
//////////////////////////////////////////////////////////

#ifndef fatjet_h
#define fatjet_h

#include "trackjet.h"
#include "muon.h"
#include "photon.h"
#include <TLorentzVector.h>

class fatjet : public TLorentzVector {
 public :
  std::vector<trackjet*> trackJetPointers;
  std::vector<photon*> photonPointers;
  std::vector<muon*> muonPointers;
  int bstatus;
  bool isTagged;
  bool isPassNtrk;
  bool isPassD2;

  fatjet();
  ~fatjet();
  void AddTrackJetPointer(trackjet * next);
  void AddPhotonPointer(photon * next);
  void AddMuonPointer(muon * next);
  trackjet * GetTrackVector(uint i);
  int  GetNumTrackVectors();
  photon * GetPhotonVector(uint i);
  int GetNumPhotonVectors();
  muon * GetMuonVector(uint i);
  int GetNumMuonVectors();
  int  GetBStatus();
  //bool isSelected();
  bool isCand();
  ClassDef(fatjet,0);

};

#endif // #ifdef fatjet_h
