#!/bin/sh
#// e.g. testRun.sh test20190513 
# void testRun(TString opdir)

date=`date +%Y%m%d`
opdir=test${date}
infile="user.jburr.2019_04_04.305442.Sherpa_CT10_ZqqGammaPt500_1000.e5020_s3126_r9364_p3780_output-zgamma.root"
outfile="ZqqGammaPt500_1000.root"

if [[ -e $opdir ]]; then
    echo $opdir already exists, removing it..
    rm -fr $opdir
fi

./Nominal $opdir $infile $outfile /data/atlas/phrsbc/qualification_task_set2/

exit
