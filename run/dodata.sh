#!/bin/sh


date=`date +%Y%m%d`
dir=Nominal"_"${date}"_dat"

if [[ -e $dir ]] ; then
   echo dir $dir already exists
   echo I quit
   exit 
fi

for thisfile in `ls -d /data/atlas/phrsbc/qualification_task_set2/user.jburr.2019_04_04.data*` ; do 

    #let counter=counter+1 
    #if [ $counter -eq 3 ]; then
    #	exit
    #fi

    infile=`echo $thisfile | awk -F/ '{print $6}'`
    outfile="Nominal-"`echo $thisfile | awk -F. '{print $4".root"}'`

    echo ${infile}
    echo ${outfile}
    filename=${outfile}"_"${date}"_dat.sh"
    echo filename=$filename
    rm -fr $filename
touch $filename
cat  <<EOF >$filename
#!/bin/sh
# Submitting for ${outfiles[$c]}
# on $date
./Nominal ${dir} ${infile} ${outfile} /data/atlas/phrsbc/qualification_task_set2/
rm -f $PWD/$filename

EOF
      chmod +x $filename
      bsub -q express -e ${outfile}"_err.txt" -o ${outfile}"_log.txt" $PWD/$filename
         (( c++ ))
done







