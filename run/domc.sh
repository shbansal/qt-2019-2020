#!/bin/sh

date=`date +%Y%m%d`
dir=Nominal"_"${date}

if [[ -e $dir ]] ; then
   echo dir $dir already exists
   echo I quit
   exit 
fi

for thisfile in `ls -d /data/atlas/yhe/QT/Vjets-ntuple/*Sherpa.DAOD_JETM6.e7048_s3126_r10201_p3916.Sherpa225.WZtag80_LCtopo-output.root`; do

    #let counter=counter+1 
    #if [ $counter -eq 3 ]; then
    #	exit
    #fi

    infile=`echo $thisfile | awk -F/ '{print $6}'`
    outfile="Nominal-"`echo $thisfile | awk -F. '{print $5"."$6".root"}'`

    echo ${infile}
    echo ${outfile}
    filename=${outfile}"_"${date}"_mc.sh"
    echo filename=$filename
    rm -fr $filename
touch $filename
cat <<EOF >$filename
#!/bin/sh
# Submitting for ${outfiles[$c]}
# on $date
./Nominal ${dir} ${infile} ${outfile} /data/atlas/yhe/QT/Vjets-ntuple/
rm -f $PWD/$filename

EOF
      chmod +x $filename
      bsub -q express -e ${outfile}"_err.txt" -o ${outfile}"_log.txt" $PWD/$filename
          (( c++ ))
done
